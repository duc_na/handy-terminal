package com.kaopiz.handyterminal.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kaopiz.handyterminal.R;

import java.util.List;

public class ListItemAdapter extends ArrayAdapter<String>{
    private List<String> listItems;
    public ListItemAdapter(@NonNull Context context, int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        this.listItems = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_item, null);
            TextView title = v.findViewById(R.id.txt_title);
            title.setText(listItems.get(position));
        }

        return v;
    }
}
