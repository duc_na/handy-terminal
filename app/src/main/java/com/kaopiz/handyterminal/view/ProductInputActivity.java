package com.kaopiz.handyterminal.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.kaopiz.handyterminal.R;

public class ProductInputActivity extends AppCompatActivity {

    private Button btnSubmit, btnSelect1, btnSelect2;
    private EditText edt1, edt2;
    private TextView txtDestination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_input);

        btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductInputActivity.this, ProductDetailActivity.class);
                intent.putExtra("DESTINATION_NAME", txtDestination.getText().toString());
                startActivity(intent);
            }
        });
        btnSelect1 = findViewById(R.id.btn_select1);
        btnSelect1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(ProductInputActivity.this, Sub1Activity.class), 1);
            }
        });
        btnSelect2 = findViewById(R.id.btn_select2);
        btnSelect2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(ProductInputActivity.this, Sub2Activity.class), 2);
            }
        });
        edt1 = findViewById(R.id.edt_num1);
        edt2 = findViewById(R.id.edt_num2);

        txtDestination = findViewById(R.id.text_input_bottom);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case 1:
                edt1.setText(data.getStringExtra("Value"));

                // set text
                if (edt2.getText().toString().isEmpty()) {
                    txtDestination.setText("一番上の届先");
                }

                break;
            case 2:
                edt2.setText(data.getStringExtra("Value"));
                String name = data.getStringExtra("Name");

                if (name != null & !name.isEmpty()) {
                    txtDestination.setText(name);
                }
                break;
        }
    }
}
