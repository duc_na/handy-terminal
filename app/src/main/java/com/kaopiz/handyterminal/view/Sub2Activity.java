package com.kaopiz.handyterminal.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kaopiz.handyterminal.R;
import com.kaopiz.handyterminal.adapter.ListItemAdapter;

import java.util.ArrayList;
import java.util.List;

public class Sub2Activity extends AppCompatActivity {
    private List<String> listItem = new ArrayList<>();
    private ListView list;
    private ListItemAdapter adapter;
    private String[] items = {"01", "58", "41", "45", "46", "50", "93", "99"};
    private String[] itemNames = {"一番上の届先", "テスト届先１", "テスト届先２", "テスト届先３", "テスト届先４", "テスト届先５", "テスト届先６", "テスト届先７"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub2);


        for (int i = 0; i < items.length; i++){
            listItem.add(items[i]+ "          " + itemNames[i]);
        }
        list = (ListView) findViewById(R.id.rv_list);
        adapter = new ListItemAdapter(this,R.layout.list_item, listItem);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = getIntent();
                intent.putExtra("Value", items[position]);
                intent.putExtra("Name", itemNames[position]);
                setResult(2, intent);
                finish();
            }
        });
    }
}
