package com.kaopiz.handyterminal.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kaopiz.handyterminal.R;
import com.kaopiz.handyterminal.adapter.ListItemAdapter;

import java.util.ArrayList;
import java.util.List;

public class Sub1Activity extends AppCompatActivity {
    private List<String> listItem = new ArrayList<>();
    private ListView list;
    private ListItemAdapter adapter;
    private String[] items = {"921600", "921700", "921800", "921900", "922400", "922500"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub1);


        for (int i = 0; i < items.length; i++){
            listItem.add(items[i]);
        }
        list = (ListView) findViewById(R.id.rv_list);
        adapter = new ListItemAdapter(this,R.layout.list_item, listItem);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = getIntent();
                intent.putExtra("Value", listItem.get(position));
                setResult(1, intent);
                finish();
            }
        });
    }
}
