package com.kaopiz.handyterminal.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.kaopiz.handyterminal.R;

public class ProductDetailActivity extends AppCompatActivity {

    private TextView txtDestination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        txtDestination = findViewById(R.id.text_1);

        String destination = getIntent().getStringExtra("DESTINATION_NAME");

        if (destination != null) {
            txtDestination.setText("届先：" + destination);
        }

    }
}
